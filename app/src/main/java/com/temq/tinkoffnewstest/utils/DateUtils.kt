package com.temq.tinkoffnewstest.utils

import java.text.DateFormat
import java.util.*
import javax.inject.Inject

class DateUtils @Inject constructor() {

    private val shortDateFormatter by lazy {
        DateFormat.getDateInstance(DateFormat.SHORT)
    }

    private val fullDateFormatter by lazy {
        DateFormat.getDateInstance(DateFormat.LONG)
    }

    private val timeFormatter by lazy {
        DateFormat.getTimeInstance(DateFormat.SHORT)
    }

    fun toDateShort(date: Date) = shortDateFormatter.format(date)

    fun toDateFull(date: Date) = fullDateFormatter.format(date)

    fun toTime(date: Date) = timeFormatter.format(date)
}