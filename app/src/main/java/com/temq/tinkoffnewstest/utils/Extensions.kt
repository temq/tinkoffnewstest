package com.temq.tinkoffnewstest.utils

import android.os.Build
import android.text.Html
import android.widget.TextView

@Suppress("DEPRECATION")

fun TextView.htmlText(text: String) {
    this.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
    } else {
        Html.fromHtml(text)
    }
}