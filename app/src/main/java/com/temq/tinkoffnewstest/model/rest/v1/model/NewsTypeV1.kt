package com.temq.tinkoffnewstest.model.rest.v1.model

enum class NewsTypeV1(val type: Int) {
    TYPE_1(1),
    TYPE_2(2);

    companion object {
        fun createByType(type: Int) = values().firstOrNull { it.type == type } ?: TYPE_1

    }
}