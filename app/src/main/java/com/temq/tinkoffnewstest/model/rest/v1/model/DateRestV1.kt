package com.temq.tinkoffnewstest.model.rest.v1.model

import com.google.gson.annotations.SerializedName
import java.util.*

class DateRestV1(@SerializedName("milliseconds") private val milliseconds: Long) {

    fun getDate() = Date(milliseconds)
}