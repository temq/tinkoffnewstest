package com.temq.tinkoffnewstest.model.rest.v1.mappers

import com.temq.tinkoffnewstest.model.entity.NewsDetail
import com.temq.tinkoffnewstest.model.rest.v1.model.NewsDetailV1
import io.reactivex.functions.Function

class NewsDetailsMapper(private val infoMapper: NewsInfoMapper,
                        private val typeMapper: NewsTypeMapper) : Function<NewsDetailV1, NewsDetail> {
    override fun apply(details: NewsDetailV1) = NewsDetail(infoMapper.apply(details.newsInfo),
            details.creationDate.getDate(),
            details.lastModificationDate.getDate(),
            details.content,
            typeMapper.apply(details.newsType),
            details.typeId)
}