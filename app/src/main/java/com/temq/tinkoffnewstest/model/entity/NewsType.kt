package com.temq.tinkoffnewstest.model.entity

enum class NewsType(val type: Int) {
    UNKNOWN(-1),
    TYPE_1(1),
    TYPE_2(2);

    companion object {
        fun createByType(type: Int): NewsType {
            return values().firstOrNull { it.type == type } ?: UNKNOWN
        }
    }
}