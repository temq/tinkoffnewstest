package com.temq.tinkoffnewstest.model.rest.v1.mappers

import com.temq.tinkoffnewstest.model.entity.NewsInfo
import com.temq.tinkoffnewstest.model.rest.v1.model.NewsInfoV1
import io.reactivex.functions.Function

class NewsInfoMapper(private val typeMapper: NewsTypeMapper) : Function<NewsInfoV1, NewsInfo> {
    override fun apply(infoV1: NewsInfoV1) = NewsInfo(infoV1.id,
            infoV1.name,
            infoV1.text,
            typeMapper.apply(infoV1.newsType),
            infoV1.publicationDate.getDate())
}