package com.temq.tinkoffnewstest.model.storage.room

import com.temq.tinkoffnewstest.model.entity.NewsDetail
import com.temq.tinkoffnewstest.model.entity.NewsInfo
import com.temq.tinkoffnewstest.model.storage.NewsStorage
import com.temq.tinkoffnewstest.model.storage.room.mappers.NewsEntityMapper
import com.temq.tinkoffnewstest.model.storage.room.mappers.NewsInfoMapper
import com.temq.tinkoffnewstest.model.storage.room.model.NewsDetailsEntity
import com.temq.tinkoffnewstest.model.storage.room.model.NewsEntity
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

class RoomNewsStorage(private val appDatabase: AppDatabase) : NewsStorage {
    override fun saveNewsInfo(newsList: List<NewsInfo>): Completable =
            Single.fromCallable { newsList.map { NewsEntity(it.id, it.name, it.text, it.newsType, it.publicationDate) } }
                    .flatMapCompletable { Completable.fromAction { appDatabase.newsDao().saveNewsInfo(it) } }


    override fun saveNewsDetails(newsDetail: NewsDetail): Completable =
            Single
                    .fromCallable {
                        val newsInfo = newsDetail.newsInfo
                        return@fromCallable NewsEntity(newsInfo.id,
                                newsInfo.name,
                                newsInfo.text,
                                newsInfo.newsType,
                                newsInfo.publicationDate,
                                NewsDetailsEntity(newsDetail.creationDate,
                                        newsDetail.lastModificationDate,
                                        newsDetail.content,
                                        newsDetail.typeId))
                    }
                    .flatMapCompletable {
                        Completable.fromAction { appDatabase.newsDao().saveNewsInfo(Collections.singletonList(it)) }
                    }


    override fun loadAllNewsInfo(): Single<List<NewsInfo>> = appDatabase.newsDao()
            .loadAllNewsInfo()
            .map {
                val mapper = NewsInfoMapper()
                it.map { mapper.apply(it) }
            }


    override fun loadNewsDetail(newsId: Long): Maybe<NewsDetail> = appDatabase.newsDao()
            .loadNewsDetail(newsId)
            .map(NewsEntityMapper())

}