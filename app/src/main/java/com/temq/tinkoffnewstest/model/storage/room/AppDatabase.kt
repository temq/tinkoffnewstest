package com.temq.tinkoffnewstest.model.storage.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.temq.tinkoffnewstest.model.storage.room.dao.NewsDao
import com.temq.tinkoffnewstest.model.storage.room.model.NewsEntity

@Database(version = 1, entities = [NewsEntity::class], exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun newsDao(): NewsDao
}