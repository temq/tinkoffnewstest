package com.temq.tinkoffnewstest.model.entity

import java.util.*

data class NewsDetail(val newsInfo: NewsInfo,
                      val creationDate: Date,
                      val lastModificationDate: Date,
                      val content: String,
                      val newsType: NewsType,
                      val typeId: String)