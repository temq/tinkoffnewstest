package com.temq.tinkoffnewstest.model.storage.room.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.temq.tinkoffnewstest.model.entity.NewsType
import java.util.*

@Entity(tableName = "news")
data class NewsEntity(
        @ColumnInfo(name = "id")
        @PrimaryKey
        var id: Long = 0,
        var name: String = "",
        var text: String = "",
        var newsType: NewsType = NewsType.TYPE_1,
        var publicationDate: Date = Date(0),
        @Embedded var details: NewsDetailsEntity = NewsDetailsEntity())