package com.temq.tinkoffnewstest.model.storage.room.mappers

import com.temq.tinkoffnewstest.model.entity.NewsInfo
import com.temq.tinkoffnewstest.model.storage.room.model.NewsEntity
import io.reactivex.functions.Function

class NewsInfoMapper : Function<NewsEntity, NewsInfo> {
    override fun apply(t: NewsEntity) = NewsInfo(t.id, t.name, t.text, t.newsType, t.publicationDate)
}