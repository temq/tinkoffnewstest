package com.temq.tinkoffnewstest.model.storage

import com.temq.tinkoffnewstest.model.entity.NewsDetail
import com.temq.tinkoffnewstest.model.entity.NewsInfo
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

interface NewsStorage {
    fun saveNewsInfo(newsList: List<NewsInfo>): Completable
    fun saveNewsDetails(newsDetail: NewsDetail): Completable
    fun loadAllNewsInfo(): Single<List<NewsInfo>>
    fun loadNewsDetail(newsId: Long): Maybe<NewsDetail>
}