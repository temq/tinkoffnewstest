package com.temq.tinkoffnewstest.model.storage.room.model

import java.util.*

data class NewsDetailsEntity(var creationDate: Date = Date(0),
                             var lastModificationDate: Date = Date(0),
                             var content: String = "",
                             var typeId: String = "")