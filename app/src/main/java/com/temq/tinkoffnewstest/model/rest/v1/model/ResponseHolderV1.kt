package com.temq.tinkoffnewstest.model.rest.v1.model

data class ResponseHolderV1<out T>(val resultCode: String,
                                   val payload: T,
                                   val trackingId: String,
                                   val errorMessage: String = "",
                                   val plainMessage: String = "")