package com.temq.tinkoffnewstest.model.rest.v1.mappers

import com.temq.tinkoffnewstest.model.entity.NewsType
import com.temq.tinkoffnewstest.model.rest.v1.model.NewsTypeV1
import io.reactivex.functions.Function

class NewsTypeMapper : Function<NewsTypeV1, NewsType> {
    override fun apply(t: NewsTypeV1): NewsType {
        return when (t) {
            NewsTypeV1.TYPE_1 -> NewsType.TYPE_1
            else -> NewsType.TYPE_2
        }
    }
}