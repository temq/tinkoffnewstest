package com.temq.tinkoffnewstest.model.rest

import com.temq.tinkoffnewstest.model.entity.NewsDetail
import com.temq.tinkoffnewstest.model.entity.NewsInfo
import io.reactivex.Single

interface NewsNetworkLoader {

    fun getNewsInfos(): Single<List<NewsInfo>>

    fun getNewsDetail(newsId: Long): Single<NewsDetail>
}