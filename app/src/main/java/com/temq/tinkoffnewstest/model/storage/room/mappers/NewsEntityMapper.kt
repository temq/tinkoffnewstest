package com.temq.tinkoffnewstest.model.storage.room.mappers

import com.temq.tinkoffnewstest.model.entity.NewsDetail
import com.temq.tinkoffnewstest.model.entity.NewsInfo
import com.temq.tinkoffnewstest.model.storage.room.model.NewsEntity
import io.reactivex.functions.Function

class NewsEntityMapper : Function<NewsEntity, NewsDetail> {
    override fun apply(it: NewsEntity): NewsDetail {
        val info = NewsInfo(it.id, it.name, it.text, it.newsType, it.publicationDate)
        val details = it.details
        return NewsDetail(info,
                details.creationDate,
                details.lastModificationDate,
                details.content,
                it.newsType,
                details.typeId)
    }
}