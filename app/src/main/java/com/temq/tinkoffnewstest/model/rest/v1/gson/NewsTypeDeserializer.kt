package com.temq.tinkoffnewstest.model.rest.v1.gson

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.temq.tinkoffnewstest.model.rest.v1.model.NewsTypeV1
import java.lang.reflect.Type

class NewsTypeDeserializer : JsonDeserializer<NewsTypeV1> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?)
            = NewsTypeV1.createByType(json?.asInt ?: NewsTypeV1.TYPE_1.type)
}