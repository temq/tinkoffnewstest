package com.temq.tinkoffnewstest.model.exception

class ApiException(val errorMessage: String, val plainMessage: String) : Exception(errorMessage)