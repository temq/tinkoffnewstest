package com.temq.tinkoffnewstest.model.rest.v1

import com.temq.tinkoffnewstest.model.entity.NewsDetail
import com.temq.tinkoffnewstest.model.entity.NewsInfo
import com.temq.tinkoffnewstest.model.exception.ApiException
import com.temq.tinkoffnewstest.model.rest.NewsNetworkLoader
import com.temq.tinkoffnewstest.model.rest.v1.mappers.NewsDetailsMapper
import com.temq.tinkoffnewstest.model.rest.v1.mappers.NewsInfoMapper
import com.temq.tinkoffnewstest.model.rest.v1.mappers.NewsTypeMapper
import com.temq.tinkoffnewstest.model.rest.v1.model.ResponseHolderV1
import io.reactivex.Single

class NewsNetworkLoaderV1(private val apiV1: NewsApiV1) : NewsNetworkLoader {

    private val typeMapper = NewsTypeMapper()
    private val infoMapper = NewsInfoMapper(typeMapper)
    private val detailsMapper = NewsDetailsMapper(infoMapper, typeMapper)

    companion object {
        const val OK = "OK"
    }

    override fun getNewsInfos(): Single<List<NewsInfo>> =
            start { getNewsInfos() }
                    .map { it.map { infoMapper.apply(it) } }

    override fun getNewsDetail(newsId: Long): Single<NewsDetail> =
            start { getNewsDetail(newsId) }
                    .map(detailsMapper)

    private fun <T> start(action: NewsApiV1.() -> Single<ResponseHolderV1<T>>): Single<T> {
        return apiV1.action()
                .flatMap {
                    if (it.resultCode == OK) {
                        return@flatMap Single.just(it.payload)
                    }
                    return@flatMap Single.error<T>(ApiException(it.errorMessage, it.plainMessage))
                }
    }
}