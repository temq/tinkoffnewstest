package com.temq.tinkoffnewstest.model.storage.room;

import android.arch.persistence.room.TypeConverter;

import com.temq.tinkoffnewstest.model.entity.NewsType;

import java.util.Date;

public class Converters {
    @TypeConverter
    public static Date toDate(long dateLong) {
        return new Date(dateLong);
    }

    @TypeConverter
    public static long fromDate(Date date) {
        return date.getTime();
    }

    @TypeConverter
    public static NewsType toNewsType(int type) {
        return NewsType.Companion.createByType(type);
    }

    @TypeConverter
    public static int fromNewsType(NewsType type) {
        return type.getType();
    }
}
