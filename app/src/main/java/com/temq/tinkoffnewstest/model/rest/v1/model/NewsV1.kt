package com.temq.tinkoffnewstest.model.rest.v1.model

import com.google.gson.annotations.SerializedName

data class NewsInfoV1(@SerializedName("id") val id: Long,
                      @SerializedName("name") val name: String,
                      @SerializedName("text") val text: String,
                      @SerializedName("bankInfoTypeId") val newsType: NewsTypeV1,
                      @SerializedName("publicationDate") val publicationDate: DateRestV1)

data class NewsDetailV1(@SerializedName("title") val newsInfo: NewsInfoV1,
                        @SerializedName("creationDate") val creationDate: DateRestV1,
                        @SerializedName("lastModificationDate") val lastModificationDate: DateRestV1,
                        @SerializedName("content") val content: String,
                        @SerializedName("bankInfoTypeId") val newsType: NewsTypeV1,
                        @SerializedName("typeId") val typeId: String)