package com.temq.tinkoffnewstest.model.rest.v1

import com.temq.tinkoffnewstest.model.rest.v1.model.NewsDetailV1
import com.temq.tinkoffnewstest.model.rest.v1.model.NewsInfoV1
import com.temq.tinkoffnewstest.model.rest.v1.model.ResponseHolderV1
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApiV1 {

    @GET("v1/news")
    fun getNewsInfos(): Single<ResponseHolderV1<List<NewsInfoV1>>>

    @GET("v1/news_content")
    fun getNewsDetail(@Query("id") newsId: Long): Single<ResponseHolderV1<NewsDetailV1>>
}