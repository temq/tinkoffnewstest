package com.temq.tinkoffnewstest.model.storage.room.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.temq.tinkoffnewstest.model.storage.room.model.NewsEntity
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveNewsInfo(newsList: List<NewsEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveNewsDetails(newsDetail: NewsEntity)

    @Query("SELECT * FROM news ORDER BY publicationDate DESC")
    fun loadAllNewsInfo(): Single<List<NewsEntity>>

    @Query("SELECT * FROM news where id = :arg0")
    fun loadNewsDetail(newsId: Long): Maybe<NewsEntity>

}