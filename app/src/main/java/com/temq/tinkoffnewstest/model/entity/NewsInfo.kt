package com.temq.tinkoffnewstest.model.entity

import java.util.*
data class NewsInfo(val id: Long,
                    val name: String,
                    val text: String,
                    val newsType: NewsType,
                    val publicationDate: Date)