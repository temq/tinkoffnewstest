package com.temq.tinkoffnewstest.ui.feed

import com.temq.tinkoffnewstest.dagger.ActivityScope
import dagger.Subcomponent

@Subcomponent
@ActivityScope
interface NewsActivityComponent {

    fun inject(newsActivity: NewsActivity)

    @Subcomponent.Builder
    interface Builder {
        fun build(): NewsActivityComponent
    }
}