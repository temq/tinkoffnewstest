package com.temq.tinkoffnewstest.ui.details

import com.temq.tinkoffnewstest.dagger.ActivityScope
import dagger.BindsInstance
import dagger.Subcomponent

@Subcomponent
@ActivityScope
interface DetailsActivityComponent {

    fun inject(detailsActivity: DetailsActivity)

    @Subcomponent.Builder
    interface Builder {
        @BindsInstance
        fun newsId(newsId: Long): Builder

        fun build(): DetailsActivityComponent
    }
}