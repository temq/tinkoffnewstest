package com.temq.tinkoffnewstest.ui

interface ViewBinder {
    fun bind()
    fun unbind()
}