package com.temq.tinkoffnewstest.ui.details

import com.temq.tinkoffnewstest.dagger.ActivityScope
import com.temq.tinkoffnewstest.ui.BaseViewModel
import com.temq.tinkoffnewstest.usecase.NewsDetailsUseCase
import com.temq.tinkoffnewstest.utils.AppSchedulers
import javax.inject.Inject

@ActivityScope
class DetailsViewModel @Inject constructor(private val newsId: Long,
                                           private val detailsUseCase: NewsDetailsUseCase,
                                           private val appSchedulers: AppSchedulers)
    : BaseViewModel<DetailsActivityBinder>() {

    override fun started(binder: DetailsActivityBinder) {
        super.started(binder)
        val disposable = detailsUseCase.loadNewsDetail(newsId)
                .subscribeOn(appSchedulers.background())
                .observeOn(appSchedulers.ui())
                .subscribe({ binder.setNewsDetail(it) }, {binder.showErrorMessageAndClose()})
        add(disposable)
    }
}