package com.temq.tinkoffnewstest.ui.feed

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.temq.tinkoffnewstest.R
import com.temq.tinkoffnewstest.utils.htmlText
import com.temq.tinkoffnewstest.model.entity.NewsInfo
import com.temq.tinkoffnewstest.utils.DateUtils
import java.util.*

class NewsAdapter(private val context: Context,
                  private val layoutInflater: LayoutInflater,
                  private val dateUtils: DateUtils)
    : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    var items: List<NewsInfo> = Collections.emptyList()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = layoutInflater.inflate(R.layout.item_news, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val (_, _, text, _, publicationDate) = items[position]
        holder!!.newsTitle.htmlText(text)
        holder.newsDate.text = dateUtils.toDateShort(publicationDate)
    }

    override fun getItemId(position: Int) = items[position].id

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val newsTitle: TextView by lazy {
            view.findViewById(R.id.news_name) as TextView
        }

        val newsDate: TextView by lazy {
            view.findViewById(R.id.news_date) as TextView
        }
    }

}