package com.temq.tinkoffnewstest.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.temq.tinkoffnewstest.App

abstract class BaseActivity : AppCompatActivity() {

    fun app() = application as App

    fun component() = app().appComponent

    companion object {
        const val RETAIN_TAG = "RETAIN_TAG"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (retainFragment() == null) {
            Log.i("BaseActivity", "Create a new fragment")
            supportFragmentManager.beginTransaction()
                    .add(RetailFragment(), RETAIN_TAG)
                    .commitNow()
        }
    }

    @Suppress("UNCHECKED_CAST")
    protected fun <T> getActivityComponent() = retainFragment()?.component as T?

    protected fun putComponent(component: Any) {
        retainFragment()?.component = component
    }

    private fun retainFragment(): RetailFragment? {
        val fragment = supportFragmentManager.findFragmentByTag(RETAIN_TAG)
        if (fragment is RetailFragment) {
            return fragment
        }
        return null
    }
}