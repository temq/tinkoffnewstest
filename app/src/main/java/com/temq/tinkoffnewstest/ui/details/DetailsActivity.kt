package com.temq.tinkoffnewstest.ui.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.temq.tinkoffnewstest.R
import com.temq.tinkoffnewstest.dagger.ActivityScope
import com.temq.tinkoffnewstest.ui.BaseActivity
import com.temq.tinkoffnewstest.utils.DateUtils
import javax.inject.Inject

class DetailsActivity : BaseActivity() {

    @ActivityScope
    @Inject
    lateinit var viewModel: DetailsViewModel

    @Inject
    lateinit var dateUtils: DateUtils

    private lateinit var binder: DetailsActivityBinder

    companion object {
        fun createIntent(context: Context, newsId: Long): Intent {
            val intent = Intent(context, DetailsActivity::class.java)
            intent.putExtra(NEWS_ID, newsId)
            return intent
        }

        private const val NEWS_ID = "NEWS_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_details)

        var component = getActivityComponent<DetailsActivityComponent>()

        if (component == null) {
            component = component().detailsActivityComponentBuilder()
                    .newsId(intent.getLongExtra(NEWS_ID, -1))
                    .build()
            putComponent(component)
        }
        component.inject(this)
        binder = DetailsActivityBinder(this, dateUtils)
    }

    override fun onStart() {
        super.onStart()
        viewModel.started(binder)
    }

    override fun onStop() {
        super.onStop()
        viewModel.stopped()
    }
}