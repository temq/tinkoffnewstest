package com.temq.tinkoffnewstest.ui.feed

import android.support.v7.util.DiffUtil
import android.util.Log
import com.temq.tinkoffnewstest.dagger.ActivityScope
import com.temq.tinkoffnewstest.model.entity.NewsInfo
import com.temq.tinkoffnewstest.ui.BaseViewModel
import com.temq.tinkoffnewstest.usecase.LoadingNewsUseCase
import com.temq.tinkoffnewstest.utils.AppSchedulers
import io.reactivex.Single
import io.reactivex.disposables.Disposables
import io.reactivex.subjects.BehaviorSubject
import java.util.*
import javax.inject.Inject

@ActivityScope
class NewsViewModel @Inject constructor(private val loadingNewsUseCase: LoadingNewsUseCase,
                                        private val appSchedulers: AppSchedulers)
    : BaseViewModel<NewsActivityBinder>(), NewsActivityBinder.NewsActivityEvents {

    private var currentNews: List<NewsInfo> = Collections.emptyList()
    private val subject: BehaviorSubject<Pair<List<NewsInfo>, DiffUtil.DiffResult?>> = BehaviorSubject.create()
    private var isStarted = false
    private var subjectDisposable = Disposables.disposed()

    override fun started(binder: NewsActivityBinder) {
        super.started(binder)
        binder.listenEvents(this)

        if (!isStarted) {
            binder.showProgress()
            loadingNewsUseCase.loadAllNews()
                    .subscribeOn(appSchedulers.background())
                    .flatMapSingle { calculateDiff(it) }
                    .subscribe({ subject.onNext(it) },
                            { Log.e("NewsViewModel", "Error updating feed", it) })

            isStarted = true
        }

        if (!subject.hasObservers()) {
            subjectDisposable = subject
                    .observeOn(appSchedulers.ui())
                    .onErrorReturnItem(Pair(currentNews, null))
                    .subscribe { handleResult(it) }
        }
    }

    override fun stopped() {
        binder?.listenEvents(null)
        super.stopped()
    }

    override fun destroyed() {
        subjectDisposable.dispose()
        subject.onComplete()
        super.destroyed()
    }

    override fun onUpdatesFeed() {
        val disposable = loadingNewsUseCase.updateNews()
                .subscribeOn(appSchedulers.background())
                .flatMap { calculateDiff(it) }
                .subscribe({ subject.onNext(it) },
                        { Log.e("NewsViewModel", "Error updating feed", it) })
        add(disposable)
    }

    override fun onNewsClicked(newsId: Long) {
        binder?.openDetail(newsId)
    }

    private fun calculateDiff(items: List<NewsInfo>) = Single.fromCallable {
        val result = NewsDiffResult(currentNews, items)
        val diffResult = DiffUtil.calculateDiff(result)
        return@fromCallable Pair(items, diffResult)
    }

    private fun handleResult(result: Pair<List<NewsInfo>, DiffUtil.DiffResult?>) {
        val localBinder = binder ?: return

        currentNews = result.first
        if (currentNews.isEmpty()) {
            localBinder.showNoNewsStub()
        } else {
            localBinder.updateNews(currentNews, result.second)
        }
        localBinder.hideProgress()
    }
}

class NewsDiffResult(private val oldItems: List<NewsInfo>, private val newItems: List<NewsInfo>)
    : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) = oldItems[oldItemPosition].id == newItems[newItemPosition].id

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = oldItems[oldItemPosition] == newItems[newItemPosition]

    override fun getOldListSize() = oldItems.size

    override fun getNewListSize() = newItems.size

}