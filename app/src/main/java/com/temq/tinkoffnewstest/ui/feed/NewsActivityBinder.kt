package com.temq.tinkoffnewstest.ui.feed

import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.util.DiffUtil
import android.support.v7.widget.*
import android.view.View
import com.temq.tinkoffnewstest.R
import com.temq.tinkoffnewstest.model.entity.NewsInfo
import com.temq.tinkoffnewstest.ui.RecyclerViewClickListener
import com.temq.tinkoffnewstest.ui.ViewBinder
import com.temq.tinkoffnewstest.ui.details.DetailsActivity
import com.temq.tinkoffnewstest.utils.DateUtils

class NewsActivityBinder(private val activity: AppCompatActivity,
                         private val dateUtils: DateUtils) : ViewBinder {

    private var listener: NewsActivityEvents? = null

    private val adapter by lazy {
        NewsAdapter(activity, activity.layoutInflater, dateUtils)
    }

    private val newsList by lazy {
        val recyclerView = activity.findViewById(R.id.news_list) as RecyclerView
        val layoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.addItemDecoration(DividerItemDecoration(activity, layoutManager.orientation))
        recyclerView.addOnItemTouchListener(RecyclerViewClickListener(activity,
                object : RecyclerViewClickListener.OnItemClick {
                    override fun onItemClick(view: View, position: Int) {
                        listener?.onNewsClicked(adapter.getItemId(position))
                    }
                }))

        return@lazy recyclerView
    }

    private val noNewsStub by lazy {
        activity.findViewById(R.id.no_news_stub) as View
    }

    private val refreshView by lazy {
        activity.findViewById(R.id.feed_refresh) as SwipeRefreshLayout
    }

    private val toolbar by lazy {
        activity.findViewById(R.id.toolbar) as Toolbar
    }

    override fun bind() {
        toolbar.title = activity.getString(R.string.news_feed)
        refreshView.setOnRefreshListener { listener?.onUpdatesFeed() }
    }

    override fun unbind() {

    }

    fun updateNews(news: List<NewsInfo>, diffResult: DiffUtil.DiffResult?) {
        newsList.visibility = View.VISIBLE
        noNewsStub.visibility = View.GONE

        adapter.items = news
        diffResult?.dispatchUpdatesTo(adapter) ?: adapter.notifyDataSetChanged()
    }

    fun showProgress() {
        refreshView.isRefreshing = true
    }

    fun hideProgress() {
        refreshView.isRefreshing = false
    }

    fun showNoNewsStub() {
        newsList.visibility = View.GONE
        noNewsStub.visibility = View.VISIBLE
    }

    fun listenEvents(newsActivityEvents: NewsActivityEvents?) {
        listener = newsActivityEvents
    }

    fun openDetail(newsId: Long) {
        activity.startActivity(DetailsActivity.createIntent(activity, newsId))
    }

    interface NewsActivityEvents {
        fun onUpdatesFeed()
        fun onNewsClicked(newsId: Long)
    }
}