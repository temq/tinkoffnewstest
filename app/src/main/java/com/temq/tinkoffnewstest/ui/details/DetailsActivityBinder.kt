package com.temq.tinkoffnewstest.ui.details

import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import android.widget.Toast
import com.temq.tinkoffnewstest.R
import com.temq.tinkoffnewstest.model.entity.NewsDetail
import com.temq.tinkoffnewstest.ui.ViewBinder
import com.temq.tinkoffnewstest.utils.DateUtils
import com.temq.tinkoffnewstest.utils.htmlText

class DetailsActivityBinder(private val activity: AppCompatActivity,
                            private val dateUtils: DateUtils)
    : ViewBinder {


    private val title by lazy {
        activity.findViewById(R.id.news_title) as TextView
    }

    private val content by lazy {
        activity.findViewById(R.id.news_details_text) as TextView
    }

    private val date by lazy {
        activity.findViewById(R.id.news_publication_date) as TextView
    }

    private val time by lazy {
        activity.findViewById(R.id.news_publication_time) as TextView
    }

    fun setNewsDetail(newsDetail: NewsDetail) {
        title.text = newsDetail.newsInfo.text
        content.htmlText(newsDetail.content)
        time.text = dateUtils.toTime(newsDetail.creationDate)
        date.text = dateUtils.toDateFull(newsDetail.creationDate)
    }

    override fun bind() {
    }

    override fun unbind() {
    }

    fun showErrorMessageAndClose() {
        Toast.makeText(activity, activity.getString(R.string.error), Toast.LENGTH_SHORT).show()
        activity.finish()
    }
}