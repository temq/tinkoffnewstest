package com.temq.tinkoffnewstest.ui

import android.support.annotation.CallSuper
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel<T : ViewBinder> {

    private val disposables = CompositeDisposable()
    protected var binder: T? = null

    @CallSuper
    open fun started(binder: T) {
        this.binder = binder
        binder.bind()
    }

    @CallSuper
    open fun stopped() {
        disposables.clear()
        binder?.unbind()
        binder = null
    }

    @CallSuper
    open fun destroyed() {
        disposables.dispose()
    }

    fun add(disposable: Disposable) {
        disposables.add(disposable)
    }
}