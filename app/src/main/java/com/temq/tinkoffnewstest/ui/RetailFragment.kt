package com.temq.tinkoffnewstest.ui

import android.os.Bundle
import android.support.v4.app.Fragment

class RetailFragment : Fragment() {

    var component: Any? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }
}