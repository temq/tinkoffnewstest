package com.temq.tinkoffnewstest.ui.feed

import android.os.Bundle
import android.util.Log
import com.temq.tinkoffnewstest.R
import com.temq.tinkoffnewstest.dagger.ActivityScope
import com.temq.tinkoffnewstest.ui.BaseActivity
import com.temq.tinkoffnewstest.utils.DateUtils
import javax.inject.Inject

class NewsActivity : BaseActivity() {

    @Inject
    @ActivityScope
    lateinit var viewModel: NewsViewModel

    @Inject
    lateinit var dateUtils: DateUtils

    private lateinit var binder: NewsActivityBinder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)

        var component = getActivityComponent<NewsActivityComponent>()
        if (component == null) {
            Log.d("NewsActivity", "Create a new component")
            component = component().newsActivityComponentBuilder()
                    .build()
            putComponent(component)
        }
        component.inject(this)
        binder = NewsActivityBinder(this, dateUtils)
    }

    override fun onStart() {
        super.onStart()
        viewModel.started(binder)
    }

    override fun onStop() {
        super.onStop()
        viewModel.stopped()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isFinishing) {
            viewModel.destroyed()
        }
    }
}
