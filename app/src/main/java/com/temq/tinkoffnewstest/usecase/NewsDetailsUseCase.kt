package com.temq.tinkoffnewstest.usecase

import com.temq.tinkoffnewstest.model.entity.NewsDetail
import com.temq.tinkoffnewstest.model.entity.NewsInfo
import com.temq.tinkoffnewstest.model.entity.NewsType
import com.temq.tinkoffnewstest.model.rest.NewsNetworkLoader
import com.temq.tinkoffnewstest.model.storage.NewsStorage
import io.reactivex.Flowable
import java.util.*
import javax.inject.Inject

class NewsDetailsUseCase @Inject constructor(private val networkLoader: NewsNetworkLoader,
                                             private val newsStorage: NewsStorage) {

    fun loadNewsDetail(newsId: Long): Flowable<NewsDetail> =
            newsStorage.loadNewsDetail(newsId)
                    .switchIfEmpty {
                        val date = Date(0)
                        val newsInfo = NewsInfo(0, "", "", NewsType.TYPE_1, date)
                        val newsDetail = NewsDetail(newsInfo, date, date, "", NewsType.TYPE_1, "")
                        it.onSuccess(newsDetail)
                    }
                    .toSingle()
                    .concatWith(networkLoader.getNewsDetail(newsId)
                            .flatMap {
                                newsStorage.saveNewsDetails(it)
                                        .toSingleDefault(it)
                            })

}