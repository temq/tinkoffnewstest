package com.temq.tinkoffnewstest.usecase

import com.temq.tinkoffnewstest.model.entity.NewsInfo
import com.temq.tinkoffnewstest.model.rest.NewsNetworkLoader
import com.temq.tinkoffnewstest.model.storage.NewsStorage
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class LoadingNewsUseCase @Inject constructor(private val networkLoader: NewsNetworkLoader,
                                             private val newsStorage: NewsStorage) {

    fun loadAllNews(): Flowable<List<NewsInfo>> =
            newsStorage.loadAllNewsInfo()
                    .concatWith(updateNews())

    fun updateNews(): Single<List<NewsInfo>> = networkLoader.getNewsInfos()
            .flatMapCompletable { newsStorage.saveNewsInfo(it) }
            .andThen(newsStorage.loadAllNewsInfo())
}