package com.temq.tinkoffnewstest.dagger;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.temq.tinkoffnewstest.model.storage.NewsStorage;
import com.temq.tinkoffnewstest.model.storage.room.AppDatabase;
import com.temq.tinkoffnewstest.model.storage.room.RoomNewsStorage;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {

    @Provides
    @Singleton
    static AppDatabase appDatabase(Context context) {
        return Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "app-database")
                .build();
    }

    @Provides
    static NewsStorage newsStorage(AppDatabase appDatabase) {
        return new RoomNewsStorage(appDatabase);
    }
}
