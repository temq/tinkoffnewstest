package com.temq.tinkoffnewstest.dagger

import com.temq.tinkoffnewstest.App
import com.temq.tinkoffnewstest.ui.details.DetailsActivityComponent
import com.temq.tinkoffnewstest.ui.feed.NewsActivityComponent
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class, NetworkModule::class, StorageModule::class])
@Singleton
interface AppComponent {

    fun newsActivityComponentBuilder(): NewsActivityComponent.Builder

    fun detailsActivityComponentBuilder(): DetailsActivityComponent.Builder

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun app(app: App): Builder

        fun build(): AppComponent
    }
}