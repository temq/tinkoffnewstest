package com.temq.tinkoffnewstest.dagger;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.temq.tinkoffnewstest.model.rest.NewsNetworkLoader;
import com.temq.tinkoffnewstest.model.rest.v1.NewsApiV1;
import com.temq.tinkoffnewstest.model.rest.v1.NewsNetworkLoaderV1;
import com.temq.tinkoffnewstest.model.rest.v1.gson.NewsTypeDeserializer;
import com.temq.tinkoffnewstest.model.rest.v1.model.NewsTypeV1;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@Singleton
public class NetworkModule {

    @Provides
    static Gson gson() {
        return new GsonBuilder()
                .registerTypeAdapter(NewsTypeV1.class, new NewsTypeDeserializer())
                .create();
    }

    @Provides
    static HttpLoggingInterceptor httpLoggingInterceptor() {
        final HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

    @Provides
    static OkHttpClient okHttpClient(HttpLoggingInterceptor loggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();
    }

    @Provides
    @Singleton
    static Retrofit retrofit(OkHttpClient client, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl("https://api.tinkoff.ru/")
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @Provides
    @Singleton
    static NewsApiV1 apiV1(Retrofit retrofit) {
        return retrofit.create(NewsApiV1.class);
    }

    @Provides
    static NewsNetworkLoader loaderV1(@NonNull NewsApiV1 apiV1) {
        return new NewsNetworkLoaderV1(apiV1);
    }
}
