package com.temq.tinkoffnewstest.dagger;

import android.content.Context;

import com.temq.tinkoffnewstest.App;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    static Context appContext(App app) {
        return app;
    }
}
